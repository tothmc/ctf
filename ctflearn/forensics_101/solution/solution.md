## forensics_101 - solution
    
Download the image. Use <code>strings</code> and <code>grep</code>.

```bash
[tothmc@arch solution]$ strings 95f6edfb66ef42d774a5a34581f19052.jpg | grep flag
flag{redacted}
[tothmc@arch solution]$ 
```

