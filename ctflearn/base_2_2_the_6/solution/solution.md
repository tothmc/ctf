## base_2_2_the_6 - solution
    
Use [CyberChef](https://cyberchef.org/) 

Convert <code>From Base64</code> and bake it.

![](solution.png)

You got the flag.