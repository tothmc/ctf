## taking_ls - solution
    
Download "The Flag.zip" from the given url.

```bash
[tothmc@arch solution]$ unzip The\ Flag.zip 
Archive:  The Flag.zip
   creating: The Flag/
  inflating: The Flag/.DS_Store      
   creating: __MACOSX/
   creating: __MACOSX/The Flag/
  inflating: __MACOSX/The Flag/._.DS_Store  
   creating: The Flag/.ThePassword/
  inflating: The Flag/.ThePassword/ThePassword.txt  
  inflating: The Flag/The Flag.pdf   
  inflating: __MACOSX/The Flag/._The Flag.pdf  
[tothmc@arch solution]$ cat The\ Flag/.ThePassword/ThePassword.txt 
Nice Job!  The Password is "Im The Flag". 
```

Open the 'The Flag.pdf' with a pdf-reader e.g. okular.

```bash
[tothmc@arch solution]$ cd The\ Flag                 
[tothmc@arch The Flag]$ ls
'The Flag.pdf'   
[tothmc@arch The Flag]$ okular The\ Flag.pdf 
[tothmc@arch The Flag]$ 
```

![](solution_01.png)

Use the found <code>password</code>.

You will find the flag in the pdf.

![](solution_02.png)