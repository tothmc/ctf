## where_can_my_robot_go - task  

### Task

https://ctflearn.com/challenge/107

![](task.png)  
    
```
Desc:Where do robots find what pages are on a website?

Hint:

What does disallow tell a robot?
```
