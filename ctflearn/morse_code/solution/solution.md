## morse_code - solution
    
Use [CyberChef](https://cyberchef.org/) 

Convert <code>From Morse Code</code> and bake it.

![](solution.png)

You got the flag.