## character_encoding - solution
    
Use [CyberChef](https://cyberchef.org/) 

Convert <code>From Hex</code> and bake it.

![](solution.png)

You got the flag.